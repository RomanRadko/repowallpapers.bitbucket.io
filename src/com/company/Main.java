package com.company;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {

    private static final String SUCCESS = "success";
    private static final String DATA = "data";
    private static int iExitValue;
    private static String sCommandString;

    public static void main(String[] args) {
        requestFindWallpapers();
    }


    static void requestFindWallpapers() {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            String getURL = "https://api.imgur.com/3/gallery/t/phone%20wallpaper";
            HttpGet get = new HttpGet(getURL);
            get.setHeader("authorization", "Client-ID 27fcf6e736ad63d");
            HttpResponse responseGet = client.execute(get);

            System.out.println(responseGet.getStatusLine().toString());

            HttpEntity resEntityGet = responseGet.getEntity();
            ArrayList<String> albumsLinksList = new ArrayList<>();
            ArrayList<String> albumsIdsList = new ArrayList<>();
            if (resEntityGet != null) {
                InputStream inputStream = resEntityGet.getContent();
                java.util.Scanner scanner = new Scanner(inputStream).useDelimiter("\\n");
                String imageObjects = scanner.next();
                System.out.println("imageObjects : " + imageObjects);
                JSONObject jsonResponseObject = new JSONObject(imageObjects);
                System.out.println("jsonResponseObject : " + jsonResponseObject);
                if (jsonResponseObject.has(SUCCESS) && !jsonResponseObject.isNull(SUCCESS)) {
                    System.out.println("Status ok!!!");
                    if (jsonResponseObject.has(DATA) && !jsonResponseObject.isNull(DATA)) {
                        System.out.println("DATA ok!!!");
                        JSONObject jsonData = jsonResponseObject.getJSONObject(DATA);
                        JSONArray items = jsonData.getJSONArray("items");
                        System.out.println("rootData : " + jsonData);
                        System.out.println("items : " + items);

                        for (int i = 0; i < items.length(); i++) {
                            albumsLinksList.add("imgur.com/a/" + items.getJSONObject(i).getString("id"));
                            albumsIdsList.add(items.getJSONObject(i).getString("id"));
                        }
                        System.out.println("Result: " + albumsLinksList);
                        runScript("imgur " + albumsLinksList);
                        File content = new File("mapping.txt");
                        for (String albumId : albumsIdsList) {
                            FileUtils.writeStringToFile(content, String.valueOf(albumId + " : "), true);
                            List<String> ids = new ArrayList<>();
                            if (Paths.get(albumId).toFile().exists()) {
                                try (Stream<Path> paths = Files.walk(Paths.get(albumId))) {
                                    paths
                                            .filter(Files::isRegularFile)
                                            .forEach(path -> ids.add(path.getName(1).toString()));
                                    FileUtils.writeStringToFile(content, ids.toString() + System.lineSeparator(), true);
                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void runScript(String command) {
        sCommandString = command;
        CommandLine oCmdLine;
        oCmdLine = CommandLine.parse(sCommandString);
        DefaultExecutor oDefaultExecutor = new DefaultExecutor();
        oDefaultExecutor.setExitValue(0);
        try {
            iExitValue = oDefaultExecutor.execute(oCmdLine);
        } catch (ExecuteException e) {
            System.err.println("Execution failed.");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Permission denied.");
            e.printStackTrace();
        }
    }

}
